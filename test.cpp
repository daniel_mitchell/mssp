#include "multiple_source_shortest_paths.hpp"
#include <cassert>
#include <iostream>

struct mssp_tests {

  typedef gsoc::detail::node<int> node;
//  using node = gsoc::detail::node<int>;

  static bool leaf(node const* u)
  { assert(u && u->up); return !u->left && !u->right && !u->path; }
  
  /*       3 -- 8 (path)
   *      / \
   *     /   \
   *    1     6
   *   / \   / \
   *  0   2 4   7
   *         \
   *          5
   */
  static node* make_bst()
  {
    node* t[9];
    for (int n = 0; n != 9; ++n) t[n] = new node, t[n]->data = n;
  
    t[5]->up = t[4];
    t[0]->up = t[2]->up = t[1];
    t[4]->up = t[7]->up = t[6];
    t[1]->up = t[6]->up = t[3];
  
    t[1]->left = t[0];
    t[6]->left = t[4];
    t[3]->left = t[1];
  
    t[4]->right = t[5];
    t[1]->right = t[2];
    t[6]->right = t[7];
    t[3]->right = t[6];
  
    t[3]->path = t[8];
  
    return t[3];
  }

  static void test_root()
  {
    node* u = make_bst();
    assert(root(u->right->left->right) == u);
  }
  
  /*       3 -- 8 (path)
   *      / \
   *     /   \
   *    1     6
   *   / \   / \
   *  0   2 4   7
   *         \
   *          5
   */
  static void test_min()
  {
    node* u = make_bst();
    assert(min(u) == u->left->left);
  }
  
  /*       3 -- 8 (path)
   *      / \
   *     /   \
   *    1     6
   *   / \   / \
   *  0   2 4   7
   *         \
   *          5
   */
  static void test_max()
  {
    node* u = make_bst();
    assert(max(u) == u->right->right);
  }
  
  /*       3 -- 8 (path)
   *      / \
   *     /   \
   *    1     6
   *   / \   / \
   *  0   2 4   7
   *         \
   *          5
   */
  static void test_next()
  {
    node* u = min(make_bst());
    assert(u->data == 0); u = next(u);
    assert(u->data == 1); u = next(u);
    assert(u->data == 2); u = next(u);
    assert(u->data == 3); u = next(u);
    assert(u->data == 4); u = next(u);
    assert(u->data == 5); u = next(u);
    assert(u->data == 6); u = next(u);
    assert(u->data == 7); u = next(u);
    assert(!u);
  }
  
  /*       3 -- 8 (path)
   *      / \
   *     /   \
   *    1     6
   *   / \   / \
   *  0   2 4   7
   *         \
   *          5
   */
  static void test_prev()
  {
    node* u = max(make_bst());
    assert(u->data == 7); u = prev(u);
    assert(u->data == 6); u = prev(u);
    assert(u->data == 5); u = prev(u);
    assert(u->data == 4); u = prev(u);
    assert(u->data == 3); u = prev(u);
    assert(u->data == 2); u = prev(u);
    assert(u->data == 1); u = prev(u);
    assert(u->data == 0); u = prev(u);
    assert(!u);
  }
  
  /*       3 -- 8 (path) |        6 -- 8 (path)
   *      / \            |       / \
   *     /   \           |      /   7
   *    1     6          |     3
   *   / \   / \         |    / \
   *  0   2 4   7        |   1   4
   *         \           |  / \   \
   *          5          | 0   2   5
   */
  static void test_rotate_left()
  {
    node* u = rotate_left(make_bst());
    assert(u->data == 6 && u->path->data == 8); u = u->left;
    assert(u->data == 3 && !u->path); u = u->left;
    assert(u->data == 1 && !u->path); u = u->left;
    assert(u->data == 0 && leaf(u)); u = u->up->right;
    assert(u->data == 2 && leaf(u)); u = u->up->up->right;
    assert(u->data == 4 && !u->left && !u->path); u = u->right;
    assert(u->data == 5 && leaf(u)); u = u->up->up->up->right;
    assert(u->data == 7 && leaf(u));
  }
  
  /*       3 -- 8 (path) |   1 -- 8 (path)
   *      / \            |  / \
   *     /   \           | 0   3
   *    1     6          |    / \
   *   / \   / \         |   2   6
   *  0   2 4   7        |      / \
   *         \           |     4   7
   *          5          |      \
   *                     |       5
   */
  static void test_rotate_right()
  {
    node* u = rotate_right(make_bst());
    assert(u->data == 1 && u->path->data == 8); u = u->left;
    assert(u->data == 0 && leaf(u)); u = u->up->right;
    assert(u->data == 3 && !u->path); u = u->left;
    assert(u->data == 2 && leaf(u)); u = u->up->right;
    assert(u->data == 6 && !u->path); u = u->left;
    assert(u->data == 4 && !u->left && !u->path); u = u->right;
    assert(u->data == 5 && leaf(u)); u = u->up->up->right;
    assert(u->data == 7 && leaf(u));
  }
  
  /*       3 -- 8 (path) |         5 -- 8 (path) |     1 -- 8 (path)
   *      / \            |        / \            |    / \
   *     /   \           |       /   \           |   /   \
   *    1     6          |      3     6          |  0     3
   *   / \   / \         |     / \     \         |       / \
   *  0   2 4   7        |    1   4     7        |      2   5
   *         \           |   / \                 |         / \
   *          5          |  0   2                |        4   6
   *                     |                       |             \
   *                     |                       |              7
   */
  static void test_splay()
  {
    node* u = make_bst();
    u = splay(u->right->left->right); // Zig-zag and zig.
    u = splay(u->left->left); // Zig-zig.
    assert(u->data == 1 && u->path->data == 8); u = u->left;
    assert(u->data == 0 && leaf(u)); u = u->up->right;
    assert(u->data == 3 && !u->path); u = u->left;
    assert(u->data == 2 && leaf(u)); u = u->up->right;
    assert(u->data == 5 && !u->path); u = u->left;
    assert(u->data == 4 && leaf(u)); u = u->up->right;
    assert(u->data == 6 && !u->left && !u->path); u = u->right;
    assert(u->data == 7 && leaf(u));
  }

  static void test_access()
  {
  }

  static void test_find_root()
  {
  }

  static void test_link()
  {
  }

  static void test_cut()
  {
  }

  static void all()
  {
    test_root();
    test_min();
    test_max();
    test_next();
    test_prev();
    test_rotate_left();
    test_rotate_right();
    test_splay();
    test_access();
    test_find_root();
    test_link();
    test_cut();
  }
};

int main()
{
  mssp_tests::all();
  std::cout << "all tests passed" << std::endl;
}
