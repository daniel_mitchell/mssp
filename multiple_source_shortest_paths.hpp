#ifndef GSOC_MULTIPLE_SOURCE_SHORTEST_PATHS_HPP
#define GSOC_MULTIPLE_SOURCE_SHORTEST_PATHS_HPP

#include <cassert>
#include <utility>

namespace gsoc {

template<typename T>
struct node {

  typedef T value_type;

  node() : up(), right(), left(), path(), data() { }

  friend node* root(node* u)
  { assert(u); while (u->up) u = u->up; return u; }

  friend node const * root(node const * u)
  { assert(u); while (u->up) u = u->up; return u; }

  friend node* min(node* u)
  { assert(u); while (u->left) u = u->left; return u; }

  friend node const * min(node const * u)
  { assert(u); while (u->left) u = u->left; return u; }

  friend node* max(node* u)
  { assert(u); while (u->right) u = u->right; return u; }

  friend node const * max(node const * u)
  { assert(u); while (u->right) u = u->right; return u; }

  node *up, *right, *left, *path;

  value_type data;
};

struct on_no_event { };

// Invoked when an up pointer is about to be changed to a path pointer during
// an access operation.
struct on_solid_to_dashed { };

// Invoked when a path pointer is about to be changed to an up pointer during
// an access operation.
struct on_dashed_to_solid { };

// Invoked when a node is linked to a parent during a link.
struct on_gain_parent { };

// Invoked when a node is unlinked from its parent during a cut.
struct on_lose_parent { };

struct null_visitor { typedef on_no_event event; };

template<typename Visitors = null_visitor>
struct mssp_visitor {

  mssp_visitor() : vis() { }

  mssp_visitor(Visitors const& vis) : vis(vis) { }

  template<typename T>
  void solid_to_dashed(node<T>* u)
  { invoke(vis, u, on_solid_to_dashed()); }

  template<typename T>
  void dashed_to_solid(node<T>* u)
  { invoke(vis, u, on_dashed_to_solid()); }

  template<typename T>
  void gain_parent(node<T>* u)
  { invoke(vis, u, on_gain_parent()); }

  template<typename T>
  void lose_parent(node<T>* u)
  { invoke(vis, u, on_lose_parent()); }

  Visitors vis;
};

namespace detail {

template<typename Visitor, typename T, typename Tag, typename Other>
inline void invoke(Visitor& vis, node<T>* u, Tag, Other) { }

template<typename Visitor, typename T, typename Tag>
inline void invoke(Visitor& vis, node<T>* u, Tag, Tag) { vis(u); }

} // namespace detail

template<typename Visitor, typename Rest, typename T, typename Tag>
inline void invoke(std::pair<Visitor,Rest>& vis, node<T>* u, Tag tag)
{
  detail::invoke(vis.first, u, tag, typename Visitor::event());
  invoke(vis.second, u, tag);
}

template<typename Visitor, typename T, typename Tag>
inline void invoke(Visitor& vis, node<T>* u, Tag tag)
{ detail::invoke(vis, u, tag, typename Visitor::event()); }

namespace detail {

template<typename T, typename Visitor>
node<T>* rotate_left(node<T>* u, Visitor vis)
{
  assert(u && u->right);

  node<T> *v = u->right, *w = v->left, *x = u->up;

  if (x) (x->right == u ? x->right : x->left) = v;

  if (w) w->up = u;

  v->up = x, v->left = u;

  u->up = v, u->right = w;

  v->path = u->path, u->path = 0;

  if (w) w->data = w->data + v->data;

  v->data = v->data + u->data;

  u->data = u->data - v->data;

  return v;
}

template<typename T, typename Visitor>
node<T>* rotate_right(node<T>* u, Visitor vis)
{
  assert(u && u->left);

  node<T> *v = u->left, *w = v->right, *x = u->up;

  if (x) (x->left == u ? x->left : x->right) = v;

  if (w) w->up = u;

  v->up = x, v->right = u;

  u->up = v, u->left = w;

  v->path = u->path, u->path = 0;

  if (w) w->data = w->data + v->data;

  v->data = v->data + u->data;

  u->data = u->data - v->data;

  return v;
}

template<typename T, typename Visitor>
inline node<T>* rotate(node<T>* u, Visitor vis)
{
  assert(u && u->up);

  u->up->right == u ? rotate_left(u->up, vis) : rotate_right(u->up, vis);

  return u;
}

template<typename T, typename Visitor>
node<T>* splay(node<T>* u, Visitor vis)
{
  assert(u);

  while (u->up)
  {
    node<T> *v = u->up, *w = v->up;

    if (!w) rotate(u, vis);

    else if (w->right && w->right->right == u || w->left && w->left->left == u)
      rotate(v, vis), rotate(u, vis);

    else rotate(rotate(u, vis), vis);
  }

  return u;
}

template<typename T, typename Visitor>
node<T>* access(node<T>* u, Visitor vis)
{
  assert(u);

  splay(u, vis);

  if (node<T>*& v = u->right) vis.solid_to_dashed(v), v->path = u, v = v->up = 0;

  while (u->path)
  {
    node<T>* v = u->path;

    splay(v, vis);

    node<T>* w = v->right;

    if (w) vis.solid_to_dashed(w), w->path = v, w->up = 0;

    vis.dashed_to_solid(u), v->right = u, u->up = v, u->path = 0;

    splay(u, vis);
  }

  assert(!u->up && !u->right);

  return u;
}

template<typename T, typename Visitor>
inline node<T>* find_root(node<T>* u, Visitor vis)
{ assert(u); return splay(min(access(u, vis)), vis); }

template<typename T, typename Visitor>
inline void link(node<T>* u, node<T>* v, Visitor vis)
{
  assert(u && !u->up && v);

  assert(find_root(u, vis) != find_root(v, vis));

  access(u, vis), access(v, vis), v->right = u, u->up = v, vis.gain_parent(u);
}

template<typename T, typename Visitor>
inline void cut(node<T>* u, Visitor vis)
{
  assert(u);

  access(u, vis);

  if (node<T>*& v = u->left) vis.lose_parent(v), v = v->up = 0;
}

} // namespace detail

namespace primal {

struct gain_parent {
  typedef on_gain_parent event;
  template<typename T>
  void operator()(node<T>* u) { u->data = u->data - u->up->data; }
};

struct lose_parent {
  typedef on_lose_parent event;
  template<typename T>
  void operator()(node<T>* u) { u->data = u->data + u->up->data; }
};

typedef mssp_visitor<std::pair<gain_parent,lose_parent> > visitor;

template<typename T>
inline void link(node<T>* u, node<T>* v) { detail::link(u, v, visitor()); }

template<typename T>
inline void cut(node<T>* u) { detail::cut(u, visitor()); }

template<typename T, typename Value>
inline void increase(node<T>* u, Value value)
{
  assert(u);

  detail::access(u, visitor());

  u->data = u->data + value;

  if (node<T>* v = u->left) v->data = v->data - value;
}

template<typename T>
inline T distance(node<T>* u)
{ assert(u); return detail::access(u, visitor())->data; }

} // namespace primal

namespace dual {

struct solid_to_dashed {
  typedef on_solid_to_dashed event;
  template<typename T>
  void operator()(node<T>* u) { u->data = u->data + u->up->data; }
};

struct dashed_to_solid {
  typedef on_dashed_to_solid event;
  template<typename T>
  void operator()(node<T>* u) { u->data = u->data - u->path->data; }
};

typedef mssp_visitor<std::pair<solid_to_dashed,dashed_to_solid> > visitor;

template<typename T>
inline void link(node<T>* u, node<T>* v) { detail::link(u, v, visitor()); }

template<typename T>
inline void cut(node<T>* u) { detail::cut(u, visitor()); }

template<typename T, typename Value>
inline void increase(node<T>* u, Value value)
{
  assert(u);

  detail::access(u, visitor());

  u->data = u->data + value;

  if (node<T>* v = u->right) v->data = v->data - value;
}

template<typename T>
inline T tension(node<T>* u)
{ assert(u); return detail::access(u, visitor())->data; }

template<typename T>
inline void max_tension(node<T>* u)
{
  assert(u);
}

} // namespace dual

//template<typename Distance, typename Size>
//struct mssp_result {
//
//  typedef Distance distance_type;
//
//  typedef Size vertices_size_type;
//
//  distance_type distance(vertices_size_type n) const
//  { return primal::distance(&primal[n]); }
//
//  vertices_size_type predecessor(vertices_size_type n) const
//  { return n == k ? n : primal::access(&primal[n])->up; }
//
//  vertices_size_type root() const { return root[k]; }
//
//  vertices_size_type next();
//
//  mssp_result(mssp_result const&);
//
//  mssp_result& operator=(mssp_result const&);
//
//private:
//
//  std::vector<node<distance_type> > primal, dual;
//
//  std::vector<vertices_size_type> root;
//
//  typename std::vector<vertices_size_type>::size_type k;
//};

} // namespace gsoc

#endif // GSOC_MULTIPLE_SOURCE_SHORTEST_PATHS_HPP
